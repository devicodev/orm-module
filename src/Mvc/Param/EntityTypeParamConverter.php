<?php
/**
 * User: granted
 * Date: 1/16/15
 * Time: 3:37 PM
 */

namespace Arilas\ORM\Mvc\Param;


use Arilas\ORM\Entity\EntityInterface;
use Arilas\ORM\EntityManager;
use Krona\CommonModule\Mvc\Param\Annotation\TypeConverter;
use Krona\CommonModule\Mvc\Param\TypeParamConverter;
use Krona\CommonModule\Reflection\ReflectionMethod;
use Krona\CommonModule\Reflection\Util\Reflector;
use Krona\CommonModule\Service\InputFilterGenerator;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractController;

class EntityTypeParamConverter extends TypeParamConverter
{
    protected function isManaged(
        \ReflectionParameter $parameter,
        TypeConverter $annotation
    ) {
        /** @var Reflector $reflector */
        $reflector = $this->getServiceLocator()->get(Reflector::class);
        $reflClass = $reflector->reflectClass($annotation->targetClass);

        return $reflClass->implementsInterface(EntityInterface::class);
    }

    protected function generateType(
        \ReflectionParameter $parameter,
        AbstractController $controller,
        ReflectionMethod $method,
        TypeConverter $annotation
    ) {
        /** @var InputFilterGenerator $typeGenerator */
        $typeGenerator = $this->getServiceLocator()->get(InputFilterGenerator::class);
        /** @var EntityManager $entityManager */
        $entityManager = $this->getServiceLocator()->get(EntityManager::class);

        /** @var Request $request */
        $request = $controller->getRequest();
        $target = $this->getTarget($controller, $annotation, $entityManager);

        $filter = $typeGenerator->createInputFilter($target, $entityManager);
        if (
            $request instanceof Request
            && in_array($request->getMethod(), [Request::METHOD_POST, Request::METHOD_PUT, Request::METHOD_PATCH])
        ) {
            $filter->setData($request->getPost()->toArray());
        }

        return $filter;
    }

    protected function getTarget(
        AbstractController $controller,
        TypeConverter $annotation,
        EntityManager $entityManager
    ) {
        $name = mb_strtolower(array_pop(explode('\\', $annotation->targetClass)));
        $target = false;

        if ($controller->params()->fromRoute($name, false)) {
            $target = $entityManager->getRepository($annotation->targetClass)->find(
                $controller->params()->fromRoute($name)
            );
        } elseif ($controller->params()->fromRoute('id', false)) {
            $target = $entityManager->getRepository($annotation->targetClass)->find(
                $controller->params()->fromRoute('id')
            );
        }
        if (!$target) {
            $target = $annotation->targetClass;
        }

        return $target;
    }
}