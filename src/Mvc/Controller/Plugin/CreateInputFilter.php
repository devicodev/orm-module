<?php
/**
 * Created by PhpStorm.
 * User: granted
 * Date: 16.08.14
 * Time: 1:16
 */

namespace Arilas\ORM\Mvc\Controller\Plugin;

use Arilas\ORM\EntityManager;
use Krona\CommonModule\Form\Type\AbstractType;
use Krona\CommonModule\Service\InputFilterGenerator;
use Zend\InputFilter\BaseInputFilter;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\Controller\PluginManager;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CreateInputFilter extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    /** @var  PluginManager */
    protected $pluginManager;
    /** @var  EntityManager */
    protected $entityManager;
    /** @var  InputFilterGenerator */
    protected $filterGenerator;
    /**
     * @param string|object $entity
     * @return BaseInputFilter|AbstractType
     * @throws \Arilas\ORM\Exception\RuntimeException
     */
    public function __invoke($entity)
    {
        return $this->filterGenerator->createInputFilter($entity, $this->entityManager);
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->pluginManager = $serviceLocator;
        $this->entityManager = $this->getServiceLocator()->getServiceLocator()->get('arilas.orm.entity_manager');
        $this->filterGenerator = $this->getServiceLocator()->getServiceLocator()->get(
            'krona.common.inputfilter.generator'
        );
    }

    /**
     * Get service locator
     *
     * @return PluginManager
     */
    public function getServiceLocator()
    {
        return $this->pluginManager;
    }
}