<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/20/13
 * Time: 4:16 PM
 */
return [
    'controller_plugins' => [
        'invokables' => [
            'getArilas' => \Arilas\ORM\Mvc\Controller\Plugin\GetArilas::class,
            'createInputFilter' => \Arilas\ORM\Mvc\Controller\Plugin\CreateInputFilter::class,
        ],
    ],
    'krona' => [
        'mvc' => [
            'converters' => [
                'abstract_converters' => [
                    [
                        'priority' => -49,
                        'converter' => \Arilas\ORM\Mvc\Param\EntityTypeParamConverter::class,
                    ],
                    [
                        'priority' => -50,
                        'converter' => \Arilas\ORM\Mvc\Param\EntityParamConverter::class,
                    ],
                ],
            ],
        ],
    ],
    'arilas' => [
        'orm' => [
            'configuration' => [
                'orm_default' => [
                    'debug' => true,
                    'cache_dir' => 'data/ArilasORM/Cache',
                ],
            ],
        ],
        'form' => [
            'em' => '',
        ],
    ],
];
