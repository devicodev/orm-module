<?php
/**
 * User: krona
 * Date: 16.01.15
 * Time: 1:22
 */

namespace Arilas\ORMTest\Mvc\Controller;


use Arilas\ORM\EntityManager;
use Arilas\ORMTest\AbstractTest;
use Arilas\ORMTest\Test\Test;
use Arilas\ORMTest\Test\TestResource;
use Krona\CommonModule\Reflection\Util\Reflector;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\Stdlib\Parameters;

class AbstractResourceTest extends \PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testExecute()
    {
        $testEntity = new Test();
        $testEntity->setValue("test");
        $testEntity->id = 1;
        self::$orm->commit($testEntity);

        $resource = new TestResource();
        $resource->setServiceLocator(static::$sm);
        $resource->getEvent()->setRouteMatch(
            new RouteMatch(
                [
                    'id' => 1
                ]
            )
        );

        $response = $resource->execute('testAction');
        $this->assertInstanceOf(Test::class, $response['test']);
        $this->assertInstanceOf(Reflector::class, $response['reflector']);
        $this->assertEquals(1, $response['id']);
        $this->assertEmpty($response['data']);
    }

    public function testPost()
    {
        $testEntity = new Test();
        $testEntity->setValue("test");
        $testEntity->id = 1;
        self::$orm->commit($testEntity);

        $event = new MvcEvent();
        $resource = new TestResource();
        $request = $resource->getRequest();
        $request->setPost(new Parameters([
            'name' => 'Test'
        ]));
        $request->setMethod(Request::METHOD_POST);

        $event->setRouteMatch(new RouteMatch([]));
        $event->setRequest($request);

        $resource->setServiceLocator(static::$sm);
        $resource->setEvent($event);
        $response = $resource->onDispatch($event);

        $this->assertInstanceOf(EntityManager::class, $response['entityManager']);
        $this->assertArrayHasKey('name', $response['data']);
    }
}
