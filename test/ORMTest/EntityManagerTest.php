<?php
/**
 * Created by PhpStorm.
 * User: gandza
 * Date: 11/27/13
 * Time: 12:41 PM
 */

namespace Arilas\ORMTest;

use Arilas\ORM\EntityManager;
use Arilas\ORM\Service\Factory;
use Arilas\ORMTest\Test\Test;
use Arilas\ORMTest\Test\TestParent;
use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Mapping\ClassMetadata;

class EntityManagerTest extends \PHPUnit_Framework_TestCase
{
    use AbstractTest;

    public function testInstance()
    {
        $factory = new Factory();
        $em = $factory->createService(static::$sm);

        $this->assertInstanceOf(EntityManager::class, $em);

        $this->assertInstanceOf(Connection::class, $em->getConnection());
        $metadata = $em->getClassMetadata(TestParent::class);
        $this->assertInstanceOf(ClassMetadata::class, $metadata);
    }

    public function testCreate()
    {
        $test = new Test();
        $test->setValue('some');
        static::$orm->commit($test);

        /** @var TestParent $parent */
        $parent = new TestParent();
        $parent->test = $test->id;

        static::$orm->commit($parent);

        /** @var TestParent $fetched */
        $fetched = static::$orm->find(TestParent::class, $parent->id);
        $this->assertInstanceOf(TestParent::class, $fetched);
        $this->assertTrue($fetched->test == $test->id);
        static::$orm->setRelatedEntity(
            $fetched,
            array(
                'test'
            )
        );
        $this->assertInstanceOf(Test::class, $fetched->test);
    }

    /**
     * @depends testCreate
     */
    public function testContains()
    {
        $entity = static::$orm->find(
            Test::class,
            1
        );

        $this->assertTrue(static::$orm->contains($entity));
        $this->assertFalse(static::$orm->contains(new Test()));
        $this->assertFalse(static::$orm->contains(new DateTime()));
        static::$orm->clear(Test::class);
        $this->assertFalse(static::$orm->contains($entity));
        $this->assertTrue(static::$orm->getRepository(TestParent::class)->getUnitOfWork()->has(1));
        static::$orm->clear();
        $this->assertFalse(static::$orm->getRepository(TestParent::class)->getUnitOfWork()->has(1));
    }
}
